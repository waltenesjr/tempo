## Tempo Online Telegram Bot

## Como usar

1. Abra o terminal e execute o comando 
    `python echobot.py`

2. Abra o telegram e procure por **@tempo_online**
3. Escreva o estado e a cidade que deseja saber a previsão do tempo.
    **Exemplo:** ***go/Goiânia***
