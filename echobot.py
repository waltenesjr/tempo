#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Simple Bot to reply to Telegram messages.

This is built on the API wrapper, see echobot2.py to see the same example built
on the telegram.ext bot framework.
This program is dedicated to the public domain under the CC0 license.
"""
import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
import tempolib as tempolib
from unicodedata import normalize

update_id = None


def main():
    """Run the bot."""
    global update_id
    # Telegram Bot Authorization Token
    bot = telegram.Bot('737411606:AAHCHhxPfgvnEs8bk5G7w0cue0HgKPv3HmI')

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1

def remover_acentos(txt):
        return normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')

def echo(bot):
    """Echo the message the user sent."""
    global update_id
    # Request updates after the last update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1

        if update.message:  # your bot can receive updates without messages
            if 'start' not in update.message.text and '/' in update.message.text:
                cidade = remover_acentos(update.message.text.lower()).replace(' ', '-')
                tempo = tempolib.consultaTempo(cidade)
                update.message.reply_text(tempo['desc'])
                update.message.reply_text('Temperatura mínima: {}°'.format(tempo['minima']))
                update.message.reply_text('Temperatura máxima: {}°'.format(tempo['maxima']))
                update.message.reply_text('Nascer do sol: {}'.format(tempo['nascer']))
                update.message.reply_text('Pôr do sol: {}'.format(tempo['por']))
                update.message.reply_text('Probabilidade de chuva: {}'.format(tempo['probabilidade']))
                update.message.reply_text('UIV máxima: {}'.format(tempo['uiv']))
            else:
                update.message.reply_text('Olá ' + update.message.chat.first_name)
                update.message.reply_text('Digite o estado e a cidade que deseja saber a previsão do tempo. \n Exemplo: go/goiania.')

if __name__ == '__main__':
    main()
