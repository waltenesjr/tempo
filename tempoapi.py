# pip install flask
from flask import Flask, render_template, request
import tempolib as tempolib
import json

app = Flask(__name__)

@app.route("/tempo", methods = ['GET'])	
def getCep():
    cidade = request.args.get('cidade')
    tempo = tempolib.consultaTempo(cidade)
    return json.dumps(tempo, ensure_ascii=False); 


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True,port=8080) 