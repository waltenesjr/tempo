from urllib.parse import urlencode
from urllib.request import Request, urlopen
import json

def unescapeXml(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    s = s.replace("&amp;", "&")
    s = s.replace("&nbsp;", " ")
    return s

def unescapeString(s):
    s = s.replace('\\r', '')
    s = s.replace('\\t', '')
    s = s.replace('\\n', '')
    return s

def getStringPosicao(result, find1, find2):
    posicao1 = int(result.index(find1) + len(find1))
    posicao2 = int(result.index(find2))
    texto = result[posicao1 : posicao2]
    return texto
    
def tirarInicio(result, find1):
    posicao1 = int(result.index(find1) + len(find1))
    texto = result[posicao1 : ]
    return texto

def processaResult(result):
    texto = getStringPosicao(result, '<div class="p-2 text-center">', '<i class="fa fa-sun-o text-warning"')
    descricao = getStringPosicao(texto, '', ".</div>")
    minima = getStringPosicao(texto, 'm&iacute;nima">', '&deg; <i class="fa fa-ther')
    maxima = getStringPosicao(texto, 'title="Temperatura m&aacute;xima">', '&deg; <i class="fa fa-thermometer-full"')
    texto = tirarInicio(texto, '<i class="fa fa-thermometer-full"')
    nascer = getStringPosicao(texto, '<span class="text-center">', '<i class="fa fa-clock-o"')
    texto = tirarInicio(texto, '<i class="fa fa-clock-o"')
    probabilidade = getStringPosicao(texto, '<span class="text-center text-primary">', '<i class="fa fa-tint"')
    texto = tirarInicio(texto, '<i class="fa fa-tint"')
    por = getStringPosicao(texto, '<span class="text-center">', '<i class="fa fa-clock-o"')
    texto = tirarInicio(texto, '<i class="fa fa-clock-o')
    uiv = tirarInicio(texto, '<span class="text-center">')
    return {'desc': descricao, 'minima': minima, 'maxima': maxima, 'nascer': nascer, 'por': por, 'probabilidade': probabilidade, 'uiv': uiv}

def consultaTempo(cidade):
    url = f'https://www.cptec.inpe.br/previsao-tempo/{cidade}'
    req =  Request(url)
    result = urlopen(req).read()
    result = str(result)
    result = unescapeString(result)
    result = bytes(result, "iso-8859-1").decode("unicode_escape")
    result = unescapeXml(result)
    return processaResult(result)